fabloom-cli
===========

Centralized fabric configuration.

Prerequisites
----------------
* python >= 2.6.x 
* virtualenv

Install
-------
Run this on your bash term.
```bash
export FABLOOM_PATH=~/.fabloom; git clone git@bitbucket.org:jonykalavera/fabloom.git $FABLOOM_PATH && virtualenv $FABLOOM_PATH/env --no-site-packages && echo "Installing requirements..." && $FABLOOM_PATH/env/bin/pip install -q -r ~/.fabloom/requirements.txt
```

Add this to your .bashrc
```bash
# Fabloom
export FABLOOM_PATH=~/.fabloom
source $FABLOOM_PATH/bin/activate.sh
# optional customization:
export FABLOOM_EDITOR='nano'  # defaults to vim.
```

Quick Start
-----------
`loom` command is just a fab wrapper.
```bash
$ loom
loom v0.1a

?: fabfile sharing.
...

# To list available commands use:
$ loom -l

# To see available options:
$ loom -h
```

Enviroment Config
-----------------

fabric.env is configured using `$FABLOOM_PATH/config.yml`

Example:
```yaml
roledefs:
   local:
     - localhost
   cluster01:
     - 'someserver:22'
     - 'other:'

# arbitrary settings can be set in yaml syntax
foo:
    bar: baz
```


Library Management
------------------
A fabloom library is a python module that contains fabric commands.

```bash
# start a new library
$ loom libs.new:my_lib
Lib:[my_lib] created at: /home/user/.fabloom/libs/my_lib.py
open with vim? [Y/n]

# or edit an existing one with $FABLOOM_EDITOR
loom libs.edit:my_lib
```