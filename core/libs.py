# coding: utf-8
from importlib import import_module
import os

from fabric.colors import red, green, blue
from fabric.contrib.console import confirm
from fabric.api import task

from .settings import LIBS_PATH, FABLOOM_EDITOR

LOADED_LIBS = []


def _load_lib(_libs_root, name):
    if name in LOADED_LIBS:
        return
    try:
        mod = import_module("libs.%s" % name)
    except Exception, exc:
        print red("== Failed to load lib: %s" % name)
        print exc
        return
    _libs_root[name]=mod
    LOADED_LIBS.append(name)

def _load_libs(_libs_root, libs_path=LIBS_PATH):
    for mod_name in os.listdir(libs_path):
        if not os.path.isdir(os.path.join(LIBS_PATH, mod_name)) and mod_name.endswith(".py"):
            _load_lib(_libs_root, mod_name[:-3])

        # mod_files = map(lambda y: y[:-3], filter(lambda x: not x.startswith("__") and x.endswith(".py"), file_list))
        # for name in mod_files:
        #         _load_lib(_libs_root, name)


def clean_name(name):
    mod_name = name.strip().lower().replace(" ", "_")
    return mod_name


@task
def edit(mod_name):
    """
    Open lib module for editing.
    """
    mod_path = os.path.join(LIBS_PATH, "%s.py" % mod_name)
    if os.path.exists(mod_path):
        os.system("%s %s" % (FABLOOM_EDITOR, mod_path))

@task
def new(name):
    """
    Start new lib module.
    """
    mod_name = clean_name(name)
    mod_path = os.path.join(LIBS_PATH, "%s.py" % mod_name)
    if not os.path.exists(mod_path):
        os.system("cat $FABLOOM_PATH/core/default_lib.tpl > $FABLOOM_PATH/libs/%s.py" % name)
        print green("Lib:[%s] created at: %s" % (mod_name, mod_path))
    else:
        print red("Lib:[%s] exists at: %s" % (mod_name, mod_path))
    if confirm("open with %s?" % FABLOOM_EDITOR):
        edit(mod_name)



