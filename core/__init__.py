# coding: utf-8
import os
from fabric.colors import red, green, blue
from fabric.api import env, task
import cuisine

from .config import _load_config
from .libs import _load_libs

__version__="0.1a"


def _set_default_environ():
    env.ssh_use_confif=True
    FABLOOM_EDITOR = os.environ.get("FABLOOM_EDITOR")
    if FABLOOM_EDITOR is None:
        FABLOOM_EDITOR = 'vim'
        os.environ["FABLOOM_EDITOR"]=FABLOOM_EDITOR
    _load_config()
    _load_libs(globals())

_set_default_environ()


@task(default=True)
def about():
    """
    About fabloom-cli.
    """
    title = "fabloom v%s" % __version__
    print(green(title))
    print("="*len(title))
    print(blue("?: fabfile hacking environment."))
    from fabric.main import show_commands
    show_commands(None, "nested")

