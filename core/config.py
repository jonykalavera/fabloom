# coding: utf-8
import os
import yaml

from fabric.api import env, task

from .settings import CONFIG_PATH, FABLOOM_EDITOR

def _load_config(conf_path=CONFIG_PATH):
    try:
        loom_config = yaml.load(file(conf_path, 'r')) or {}
    except yaml.YAMLError, exc:
        print "Error in configuration file:", exc
    for key, value in loom_config.items():
    	setattr(env, key, value)


@task(default=True)
def edit():
	"""
	Open config.yml for editing.
	"""
	os.system("%s %s" % (FABLOOM_EDITOR, CONFIG_PATH))
