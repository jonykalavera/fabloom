# loom library
from fabric.colors import red, green, blue
from fabric.contrib.console import confirm
from fabric.api import task, env
from cuisine import run


@task(default=True)
def hello():
    """
    Say hello!
    """
    print(green("hello cuisine!"))
    if confirm("run uname -a?"):
    	run("uname -a")
