import os

FABLOOM_PATH = os.environ.get('FABLOOM_PATH', "$HOME/.fabloom")
FABLOOM_EDITOR = os.environ.get('FABLOOM_EDITOR', "vim")
CONFIG_PATH = os.path.join(FABLOOM_PATH, 'config.yml')
LIBS_PATH = os.path.join(FABLOOM_PATH, 'libs')


